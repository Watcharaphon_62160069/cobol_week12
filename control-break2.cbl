       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CB2.
       AUTHOR. WATCHARAPHON.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT INPUT2-FILE ASSIGN TO "input2.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT2-FILE.
       01  INPUT2-BUFFER.
           88 END-OF-INPUT1-FILE         VALUE    HIGH-VALUE.
           05 COL-A          PIC XX.
           05 COL-B          PIC XX.
           05 COL-COUNT      PIC 999.
       WORKING-STORAGE SECTION. 
       01  TOTAL             PIC 9999    VALUE ZERO.
       01  COL-A-TOTAL       PIC 9999.
       01  COL-A-PROCESSING  PIC XX.
       01  COL-B-TOTAL       PIC 9999.
       01  COL-B-PROCESSING  PIC XX.

       01  RPT-HEADER.
           05 FILLER         PIC XXXX   VALUE "  A ".
           05 FILLER         PIC XXXX   VALUE SPACE.
           05 FILLER         PIC XXXX   VALUE "   B".
           05 FILLER         PIC XXXX   VALUE SPACE.
           05 FILLER         PIC X(6)   VALUE " TOTAL".

       01  RPT-ROW.
           05 RPT-COL-A      PIC BBXX.
           05 FILLER         PIC XXXXX   VALUE SPACE.
           05 RPT-COL-B      PIC BBXX.
           05 FILLER         PIC XXXXX   VALUE SPACE.
           05 RPT-COL-TOTAL  PIC ZZZ9.
       01  RPT-A-TOTAL-ROW.
           05 FILLER         PIC X(7)   VALUE "      ".
           05 FILLER         PIC X(11)   VALUE "    TOTAL: ".
           05 RPT-A-TOTAL    PIC ZZZ9.
       01  RPT-FOOTER.
           05 FILLER         PIC X(9)   VALUE "  TOTAL :".
           05 RPT-TOTAL      PIC ZZZ9.

       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT INPUT2-FILE 
           DISPLAY RPT-HEADER 
           PERFORM READ-LINE
           PERFORM PROCESS-COL-A UNTIL END-OF-INPUT1-FILE 
           MOVE TOTAL TO RPT-TOTAL
           DISPLAY RPT-FOOTER  
           CLOSE INPUT2-FILE 
           GOBACK 
       .      
       PROCESS-COL-A.
           MOVE COL-A TO COL-A-PROCESSING  
           MOVE COL-A-PROCESSING  TO RPT-COL-A 
           MOVE ZERO TO COL-A-TOTAL
           PERFORM PROCESS-COL-B UNTIL COL-A NOT = COL-A-PROCESSING
           MOVE COL-A-TOTAL TO RPT-A-TOTAL  
           DISPLAY RPT-A-TOTAL-ROW 
       .
       PROCESS-COL-B.
           MOVE COL-B TO COL-B-PROCESSING  
           MOVE ZERO TO COL-B-TOTAL
           PERFORM PROCESS-LINE UNTIL COL-B NOT = COL-B-PROCESSING
              OR COL-A NOT = COL-A-PROCESSING
           MOVE COL-B-PROCESSING  TO RPT-COL-B 
           MOVE COL-B-TOTAL TO RPT-COL-TOTAL 
           DISPLAY RPT-ROW 
           MOVE SPACE TO RPT-COL-A
       .
       PROCESS-LINE.
           ADD COL-COUNT TO TOTAL, COL-A-TOTAL ,COL-B-TOTAL 
           PERFORM READ-LINE 
       .
       READ-LINE.
           READ INPUT2-FILE
              AT END SET END-OF-INPUT1-FILE TO TRUE
           END-READ
       .
